import day3_input

current_pos=[0,0]
current_robo_pos=[0,0]

positions=[]
Santas_Turn=True


for i in day3_input.input:
    if i == "^":
        if Santas_Turn:
            current_pos[1]+=1
        else:
            current_robo_pos[1]+=1
    elif i == "v":
        if Santas_Turn:
            current_pos[1]-=1
        else:
            current_robo_pos[1]-=1
    elif i == ">":
        if Santas_Turn:
            current_pos[0]+=1
        else:
            current_robo_pos[0]+=1
    elif i == "<":
        if Santas_Turn:
            current_pos[0]-=1
        else:
            current_robo_pos[0]-=1

    if Santas_Turn:
        positions.append(str(current_pos))
        Santas_Turn = False
    else:
        positions.append(str(current_robo_pos))
        Santas_Turn = True

print len(set(positions))

print []