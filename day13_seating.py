import itertools
input = open("day13_input").read().split('\n')

conditions=[]
for i in input:
    condi={}
    split=i.split(' ')
    condi['name']=split[0]
    try:
        condi['value']=int(split[3])
    except:
        pass
    if split[2] == 'lose':
        condi['value']*=-1
    condi['other']=split[-1][:-1]
    conditions.append(condi)

names = list(set([x['name'] for x in conditions]))

lowest=0
for combi in itertools.permutations(names):
    value=0
    for nr, c in enumerate(combi):
        if nr==0:
            prev = combi[-1]
        else:
            prev = combi[nr-1]

        if nr==len(combi)-1:
            next=combi[0]
        else:
            next = combi[nr+1]

        left=[x['value'] for x  in conditions if x['name'] == c and x['other']==prev][0]
        right=[x['value'] for x in conditions if x['name'] == c and x['other']==next][0]
        value+=left
        value+=right
    if value > lowest:
        lowest=value
        t=combi

    print lowest

print t