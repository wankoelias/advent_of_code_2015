import numpy as np
import scipy.misc
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import time


def animate(lights, continous=False, steps=0):

    overlay = lights.copy()
    zero_rows = np.zeros(overlay.shape[1], int)
    overlay = np.vstack((zero_rows, overlay, zero_rows))
    zero_columns = np.zeros(overlay.shape[0], int).reshape(overlay.shape[0], 1)
    overlay = np.hstack((zero_columns, overlay, zero_columns))
    if continous:
        overlay[0]=overlay[-2]
        overlay[-1]=overlay[1]

        overlay[:,0]=overlay[:,-2]
        overlay[:,-1]=overlay[:,1]

    counter = np.zeros(lights.shape, int)
    counter += overlay[2:, 1:-1] + overlay[2:, :-2] + overlay[1:-1, :-2] + overlay[:-2, :-2]
    counter += overlay[:-2, 1:-1] + overlay[:-2, 2:] + overlay[1:-1, 2:] + overlay[2:, 2:]

    lights[(((lights == 1)*(counter+1)) > 0) & (((lights == 1)*(counter+1)) < 3) | (((lights == 1)*(counter+1)) > 4)] = 0
    lights[((lights == 0)*(counter+1)) == 4] = 1

    # lights[(((lights == 1)*(counter+1)) > 0) & (((lights == 1)*(counter+1)) != 3) & (((lights == 1)*(counter+1)) != 4)] = 0
    # lights[((lights == 0)*(counter+1)) == 4] = 1

    # lights[(((lights == 1)*(counter+1)) > 0) & (((lights == 1)*(counter+1)) > 0)] = 0
    # lights[(((lights == 0)*(counter+1)) > 0) & ((((lights == 0)*(counter+1)) < 2) | (((lights == 0)*(counter+1)) > 5))] = 1

    # lights[0, 0] = 1
    # lights[0, -1] = 1
    # lights[-1, 0] = 1
    # lights[-1, -1] = 1

    steps += 1

    scipy.misc.toimage(lights).save(r".\png2\%s.png"%(steps), "png")

    return lights


raw = open("day18_input").read().split('\n')
lights_init = np.zeros((len(raw), len(raw[0])), int)
for line_nr, line in enumerate(raw):
    line = line.replace("#", '1').replace('.', '0')
    for column, value in enumerate(line):
        lights_init[line_nr, column] = int(value)

#
# lights_init[0, 0] = 1
# lights_init[0, -1] = 1
# lights_init[1, 0] = 1
# lights_init[1, -1] = 1
#
# lights_init = np.hstack(tuple([lights_init]*3))
# lights_init = np.vstack(tuple([lights_init]*3))

lights_init = np.vstack((np.zeros(lights_init.shape), lights_init, np.zeros(lights_init.shape)))
lights_init = np.hstack((np.zeros(lights_init.shape), lights_init, np.zeros(lights_init.shape)))

for n in xrange(10000):
    lights_init = animate(lights_init, steps=n, continous=True)

print np.sum(lights_init)
