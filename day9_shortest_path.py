import itertools

input = open("day9_input.txt").read().split('\n')
paths = [[i.split(' ')[0], i.split(' ')[2], int(i.split(' ')[-1])] for i in input]
destinations = list(set([x[0] for x in paths]+[x[1] for x in paths]))
min, max = float("inf"), 0
for nored in itertools.permutations(destinations):
    distance=0
    for p in xrange(len(nored)-1): distance += [path for path in paths if nored[p] in path and nored[p+1] in path][0][2]
    if distance < min: min = distance
    if distance > max: max = distance
print min, max
