import random
import re

raw = open("day19_input").read().split('\n')
molecule = raw[-1]
replacements = [r.split(' => ') for r in raw[:-2]]

print molecule

molecules =[]

for r in replacements:
    for nr, _ in enumerate(molecule):
        if molecule[nr:nr+len(r[0])] == r[0]:
            molecules.append(molecule[:nr]+r[1]+molecule[nr+len(r[0]):])

print len(list(set(molecules)))

fastest = 9999
fail = 0
fails=[]
while len(fails)<20:
    mol = molecule
    steps = 0
    real_steps=0
    while True:
        steps +=1
        final = [h[1] for h in replacements if h[0] == "e"]
        if mol not in final:
            repid = random.randint(0, len(replacements)-3)
            if replacements[repid][1] in mol:
                real_steps+=1
                mol=mol.replace(replacements[repid][1], replacements[repid][0], 1)
        else:
            real_steps+=1
            if real_steps <= fastest:
                fastest = real_steps
                fails.append(fail)
                print mol+": " + str(fastest) + " - " + str(sum(fails)/len(fails)) + " - " + str(len(fails))
                fail=0
            break
        if steps >= 1000:
            fail+=1
            break
