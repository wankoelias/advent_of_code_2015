import re
deers=[]
for r in open("day14_input.txt").read().split('\n'):
    deers.append([re.findall("[A-z]+", r)[0]]+[int(x) for x in re.findall("[0-9]+", r)])

timed=[]

for deer in deers:
    cycles = 2503/(deer[2]+deer[3])
    rest=2503%(deer[2]+deer[3])
    if rest < deer[2]:
        timed.append((cycles*deer[2]+rest)*deer[1])
    else:
        timed.append(((cycles+1)*deer[2])*deer[1])
print max(timed)

deers = [{'name': d[0], 'speed':d[1], 'flytime':d[2], 'resttime':d[3], 'points':0, 'distance':0, 'status':0} for d in deers]

for t in xrange(2503):
    for id, deer in enumerate(deers):
        if deer['status']<deer['flytime'] and deer['status']>=0:
            deers[id]['distance']+=deer['speed']
            deers[id]['status']+=1
        elif deer['status']<deer['flytime'] and deer['status']<=0:
            deers[id]['status']+=1
        elif deer['status']==deer['flytime']:
            deers[id]['status']=(deer['resttime']-1)*(-1)

    farthest = max([d['distance'] for d in deers])
    for id, deer in enumerate(deers):
        if deer['distance'] == farthest: deers[id]['points']+=1

    print [(d['name'], d['distance']) for d in deers]
    # print t+1, [[d['name'], d['status'], d['distance'], d['points']] for d in deers]

print max([d['points'] for d in deers])
