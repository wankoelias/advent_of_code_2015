import itertools
from collections import Counter

raw = open("day24_input").read().split('\n')

packages = [int(i) for i in raw]
packages = [1, 2, 3, 6, 7, 8, 9, 11, 16]

group_weight = sum(packages)/4

def c_tolist(count):
    l = list()

    for k, c in count.iteritems():
        l += [k]*c

    return l

def get_qe(group):
    qe = 1
    for g in group:
        qe *= g
    return qe

def get_combinations(weights):
    for l in range(len(weights)):
        for co in itertools.combinations(weights, l+1):
            yield co

def divisible(group, weights, target):
    rest = c_tolist(Counter(weights)-Counter(group))
    min_qe = get_qe(group)
    div = False

    for group_2 in get_combinations(rest):
        if sum(group_2) == target:
            rest_2 = c_tolist(Counter(rest)-Counter(group_2))

            for group_3 in get_combinations(rest_2):
                if sum(group_3) == target:

                    div = True
                    group_4 = c_tolist(Counter(rest_2)-Counter(group_3))

                    min234 = min([get_qe(g) for g in [group_2, group_3, group_4]])

                    if min_qe > min234:
                        min_qe = min234
    if div:
        return min_qe
    else:
        return False


combinations = get_combinations(packages)

qemin = 9999999999999999999999999

for a in combinations:
    if sum(a) == group_weight:

        curr_min = divisible(a, packages, group_weight)

        if curr_min:
            if curr_min < qemin:
                qemin = curr_min
                print a
                print "New Minimum qe: {}".format(qemin)


print(i)
