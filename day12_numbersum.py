import re
nored=""


def makesum(input):
    return sum(map(int, re.findall("[0-9-]+", input)))


def fillnored(x):
    if isinstance(x, dict):
        if 'red' not in x.values():
            return map(fillnored, [d for d in x.iteritems()])
    elif isinstance(x, list) or isinstance(x, tuple):
        return map(fillnored, [d for d in x])
    else:
        global nored
        nored += '_'+str(x)

print makesum(open("day12_input").read())
fillnored(eval(open("day12_input").read()))
print makesum(nored)
