coderow=2946
codecolumn=3028
size=coderow+codecolumn+1

current=20151125

def calc(c):
    return int(float(252533*c)%33554393)

for diagonal in xrange(1, size):
    column=0
    row=diagonal

    current= calc(current)
    while column < diagonal:
        column+=1
        row-=1
        current=calc(current)
        if row==coderow and column==codecolumn:
            print current
