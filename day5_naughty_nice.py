import day5_input

nicecount=0
for i in day5_input.input.split("\n"):
    Nicecluster=False
    Nicegap=False
    if i == '':
        continue

    for x_pos, x in enumerate(i):
        clusters=[]
        if x_pos >1:
            if i[x_pos-2]==x:
                Nicegap=True

        if  x_pos+1 == len(i):
            continue

        pair=x+i[x_pos+1]
        if i.count(pair)>1:
            Nicecluster=True

    if Nicegap and Nicecluster:
        nicecount+=1
print nicecount
