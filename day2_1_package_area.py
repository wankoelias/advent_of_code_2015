import day2_1_input


input_list = day2_1_input.input.split('\n')

area=0
length=0
for p in input_list:

    if not p=='':
        sides=p.split('x')
        sides=map(int, sides)
        areas=[]
        areas.append(sides[0]*sides[1]*2)
        areas.append(sides[0]*sides[2]*2)
        areas.append(sides[1]*sides[2]*2)
        areas.append(min(areas)/2)
        area+=sum(areas)

        side1=min(sides)
        side2=9999999
        if sides.count(side1)>1:
            side2=side1
        else:
            for s in sides:
                if s != side1:
                    if s<side2:
                        side2=s

        length+=2*side1+2*side2+sides[0]*sides[1]*sides[2]


print area
print length