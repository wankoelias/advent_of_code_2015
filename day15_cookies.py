import re
import operator
import itertools

items = [{a:(lambda b: int(b) if len(b) <= 2 else b)(b) for a, b in
        dict([["name", re.findall(".+:", line)[0][:-1]]] + [e.split(' ') for e in re.findall("[a-z]+\s[0-9-]+", line)]).iteritems()}
        for line in open("day15_input").read().split('\n')]

combinations =list(set([x for x in itertools.combinations([x+1 for x in range(100)]*4, 4) if sum(x)==100]))

# combinations = eval(open('day_15_combinations').read())
print max([(lambda x: reduce(operator.mul, x, 1))([(lambda x: x if x>0 else 0)(sum(k)) for k in
        zip(*[[i[0]*b for n, b in i[1].iteritems() if n!="name" and n!="calories"] for i in
        zip(c, items)])]) for c in combinations])

print max([f[1] for f in [(sum([p[0]*p[1] for p in zip([z['calories'] for z in items], c)]),
        (lambda x: reduce(operator.mul, x, 1))([(lambda x: x if x>0 else 0)(sum(k)) for k in
        zip(*[[i[0]*b for n, b in i[1].iteritems() if n!="name" and n!="calories"] for i in zip(c, items)])]))
        for c in combinations] if f[0] == 500])
