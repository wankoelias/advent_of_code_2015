import day6_input
import numpy as np

lights=np.zeros((1000,1000), int)
input_list=day6_input.input.split('\n')
inputs = [x.split(' ') for x in input_list]
for i in inputs:
    if i == ['']: continue
    if i[0]=="turn": del i[0]
    start=map(int, i[1].split(','))
    stop=map(int, i[3].split(','))
    if i[0] == 'toggle': lights[start[0]:stop[0]+1, start[1]:stop[1]+1] +=2
    elif i[0] == 'on': lights[start[0]:stop[0]+1, start[1]:stop[1]+1] += 1
    elif i[0] == 'off':
        select=lights[start[0]:stop[0]+1, start[1]:stop[1]+1]
        lights[start[0]:stop[0]+1, start[1]:stop[1]+1]   += (select > 0) * -1

print lights.sum()