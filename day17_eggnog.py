from itertools import combinations as c
s=map(int, open("day17_input.txt").read().split('\n'))
print sum([len([a for a in c(s, i) if sum(a)==150]) for i in range(len(s))])
print min([len(x) for x in[[len(a) for a in c(s, i) if sum(a)==150] for i in range(len(s))] if len(x)>0])