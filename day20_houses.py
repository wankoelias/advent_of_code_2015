def factors(n):
    return set(reduce(list.__add__,
                ([i, n//i] for i in range(1, int(n**0.5) + 1) if n % i == 0)))


for house in xrange(10000000):
    house=house+1
    fact=[f*11 for f in list(factors(house)) if house/f <= 50]
    h=sum(fact)

    if house % 10000 == 0: print house, h
    if h>=33100000:
        print house, h
        print "finished"
        break

