import re
sues=[]
for i in open("day16_input.txt").read().split('\n'):
    sues.append(dict({x[0]:int(x[1]) for x in [a.split(': ') for a in re.findall("[S-z]+:\s[0-9]", i)]}.items()+[('nr', re.findall("[0-9]+", i)[0])]))

def chk(s, name, value):
    if name in s.keys():
        if name in ["cats", "trees"]: return s[name]>value
        elif name in ["pomeranians", "goldfish"]: return s[name]<value
        else: return s[name]==value
    else: return True

print [s["nr"] for s in sues if
chk(s,"children",3) and chk(s,"cats",7) and chk(s,"samoyeds",2) and chk(s,"pomeranians",3) and chk(s,"akitas",0) and
chk(s,"vizslas",0) and chk(s,"goldfish",5) and chk(s,"trees",3) and chk(s,"cars",2) and chk(s,"perfumes",1)]