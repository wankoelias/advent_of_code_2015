__author__ = 'Elias'

raw = open("day23_input").read().split('\n')
instructions = [i.replace(",", "").split() for i in raw]

registers = {"a":1, "b":0}

pos = 0

while len(instructions) >= pos+1:
    i = instructions[pos]

    if i[0] == "hlf":
        registers[i[1]] *= 0.5
    elif i[0] == "tpl":
        registers[i[1]] *= 3
    elif i[0] == "inc":
        registers[i[1]] += 1
    elif i[0] == "jmp":
        pos += int(i[1][1:])*int(i[1][0]+"1")
        continue
    elif i[0] == "jie":
        if registers[i[1]]%2 == 0:
            pos += int(i[2][1:])*int(i[2][0]+"1")
            continue
    elif i[0] == "jio":
        if registers[i[1]] == 1:
            pos += int(i[2][1:])*int(i[2][0]+"1")
            continue
    pos += 1

print registers