import re

def part1():
    file=open("day8_input.txt")
    count = 0
    for f in file:
        f= f[:-1]
        count += len(f)
        for x in re.findall(r"\\x[0-9]{2}", f):
            f.replace(x, x[-2:].decode("hex"))
        count -= len(eval(f))
    return count

def part2():
    file=open("day8_input.txt")
    count = 0
    for f in file:
        f= f[:-1]
        original = len(f)
        count -= original
        for a in ["\\", '\"', "\'"]:
            f=f.replace(a, "\\"+a)
        f='"'+f+'"'
        new = len(f)
        count += new
        pass
    return count
print part1()
print part2()

