from itertools import combinations

maxgold=0
best=None

def fight(cost, damage, defence, maxgold):
    me=[100, damage, defence]
    boss=[109, 8, 2]
    winner=None

    while True:
        medmg=me[1]-boss[2]
        if medmg<1:medmg=1
        bossdmg=boss[1]-me[2]
        if bossdmg<1:bossdmg=1

        boss[0]-=medmg
        if boss[0]<=0:
            winner="me"
            break
        me[0]-=bossdmg
        if me[0]<0:
            winner="boss"
            break
    if winner == "boss" and cost>maxgold:
        global best
        best=[armor, ring, weapon]
        return cost
    else:
        return maxgold

weapons=[[8,4,0],[10,5,0],[25,6,0],[40,7,0],[74,8,0]]
armors=[[13,0,1],[31,0,2],[53,0,3],[75,0,4],[102,0,5]]
rings=[[25,1,0],[50,2,0],[100,3,0],[20,0,1],[40,0,2],[80,0,3]]

print

for weapon in weapons:
    for armor in armors+[[0,0,0]]:
        for ring in [list(i) for i in combinations(rings, 2)]+[[r] for r in rings]+[[[0,0,0]]]:
            ringcost=sum([r[0] for r in ring])
            cost=weapon[0]+armor[0]+ringcost
            damage=weapon[1]+sum([r[1] for r in ring])
            defence=armor[2]+sum([r[2] for r in ring])
            maxgold=fight(cost, damage, defence, maxgold)

print maxgold, best