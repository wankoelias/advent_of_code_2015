input = open("day7_input.txt").read()
for k in ["as", "is", "id", "if", "in", "or"]: input=input.replace(k, k+'_')
input=input.replace(" AND ", '&')
input=input.replace(" OR ", '|')
input=input.replace("NOT ", '~')
input=input.replace(" LSHIFT ", '<<')
input=input.replace(" RSHIFT ", '>>')
input=input.split('\n')
def work(b_input=None):
    a=None
    while not a:
        for row in input:
            try:
                split=row.split(' -> ')
                exec(("%s = %s" % (split[1], split[0])))
                if b_input:
                    b = b_input
            except NameError: pass
    return a
nored = work()
print nored
print work(nored)