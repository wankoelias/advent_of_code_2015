class vigintisex():

    def __init__(self, input):
        self.CHARS = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
        if str(input)[0] in self.CHARS:
            self.vig=input
            self.todec()
        else:
            self.dec=input
            self.tovig()

    def todec(self):
        value=self.vig
        self.dec=0
        for i in range(len(value)):
            self.dec+=pow(26, i)*self.CHARS.index(value[-i-1])

    def tovig(self):
        self.vig=""
        value=self.dec
        base=1
        while (base*26) <= self.dec:
            base*=26

        while base != 0:
            div=value/base
            value=value%base
            self.vig+=self.CHARS[div]
            base /= 26

    def __add__(self, other):
        self.dec+=other
        self.tovig()
        return self

    def __str__(self):
        return self.vig



def convert(value, system_src, system_target):
    CHARS_num= ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z", " "]
    CHARS_abc = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]

    # check if input is abc
    if str(system_src)=="abc":
        CHARS_src=CHARS_abc
        system_src=26
    else:
        CHARS_src=CHARS_num
    if str(system_target)=="abc":
        CHARS_target=CHARS_abc
        system_target=26
    else:
        CHARS_target=CHARS_num

    #convert input to decimal
    decimal=0
    value=str(value)

    for i in range(len(value)):
        decimal+=pow(system_src, i)*CHARS_src.index(value[-i-1])

    value=decimal

    # Convert decima to output system
    base=1
    ret=""
    while (base*system_target) <= value:
        base*=system_target

    while base != 0:
        div=value/base
        value=value%base
        ret+=CHARS_target[div]
        base /= system_target

    if system_target <=10: return int(ret)
    else: return ret




pw="cqjxxzab"
overpasses=0
abcpasses=0
letterpasses=0

while True:
    letterpass=True
    abcpass=False
    overpass=False
    for x in ["i", "o", "l"]:
        if x in pw:
            letterpass= False
    if letterpass:
        letterpasses+=1

    over=[]
    for i in range(len(pw)-2):
        current=ord(pw[i])
        if pw[i+1]==chr(current+1) and pw[i+2]==chr(current+2):
            abcpass=True
            abcpasses+=1

    for i in range(len(pw)-1):
        if pw[i] == pw[i+1]:
            if pw[i] not in over:
                if i !=0 and pw[i-1] != pw[i]:
                    over.append(pw[i])
                elif i== 0:
                    over.append(pw[i])
    if len(over)>=2:
        overpass=True
        overpasses+=1

    if letterpass and abcpass and overpass: break
    pw = convert(convert(pw, "abc", 10)+1, 10, "abc")

print pw

